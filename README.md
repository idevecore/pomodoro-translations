# Translations

[![Translation status](https://hosted.weblate.org/widget/pomodoro/pomodoro/svg-badge.svg)](https://hosted.weblate.org/engage/pomodoro/) ✨Powered by [Weblate](https://weblate.org/en/)✨

This repository is intended only for Pomodoro translations

Pomodoro has been translated into the following languages:

<a href="https://hosted.weblate.org/engage/pomodoro/">
<img src="https://hosted.weblate.org/widget/pomodoro/pomodoro/multi-auto.svg" alt="Translation status" />
</a>

Please help translate Pomodoro into more languages through [Weblate](https://hosted.weblate.org/engage/pomodoro/).